'use strict';

import App from './app/App.vue';
import Routing from './routes/page';
import bootstrap from './bootstrap';
bootstrap(App, Routing.routes);