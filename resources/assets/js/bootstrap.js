import configRouter from './routes';

// create router
export var router = new VueRouter({
  history:            true,
  saveScrollPosition: true
});

export default function(app, routes) {
  // register directives globally
  // require('./directives');

  Vue.config.debug = process.env.NODE_ENV !== 'production';

  // inject the routes into the VueRouter object
  configRouter(router, routes);

  // bootstrap the App
  // Vue.component('component', Component);

  router.start(app, '#app');
}
