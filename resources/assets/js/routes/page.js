import MainPage from '../components/MainPage.vue';

export default {
  routes: {
    '/': {
      component: MainPage,
      name:      'main-page'
    }
  }
}
