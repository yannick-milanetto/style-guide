'use strict';

var gulp = require('gulp');  // Base gulp package

var babelify   = require('babelify'); // Used to convert ES6 & JSX to ES5
var browserify = require('browserify'); // Providers "require" support, CommonJS
var vueify     = require('vueify'); // Used to convert Vue components to ES6 (ES5 ?)
var watchify   = require('watchify'); // Watchify for source changes

var uglify     = require('gulp-uglify'); // Minify JS
var sass       = require('gulp-sass'); // Convert SASS into CSS
var rename     = require('gulp-rename'); // Rename sources
var sourcemaps = require('gulp-sourcemaps'); // Provide external sourcemap files
var livereload = require('gulp-livereload'); // Livereload support for the browser
var gutil      = require('gulp-util'); // Provides gulp utilities, including logging and beep
var duration   = require('gulp-duration'); // Time aspects of your gulp process

var chalk  = require('chalk'); // Allows for coloring for logging
var source = require('vinyl-source-stream'); // Vinyl stream support
var buffer = require('vinyl-buffer'); // Vinyl stream support
var concat = require('gulp-concat'); // Concatenate some files in one
var merge  = require('utils-merge'); // Object merge tool
//var es = require('event-stream'); // Allows to catch stream events

process.env.NODE_ENV = 'development';
//process.env.NODE_ENV = 'production';

// Configuration for Gulp
let config = {
  js:         {
    src: './resources/assets/js',
    dst: './public_html/assets/js'
  },
  css:        {
    src: './resources/assets/sass',
    dst: './public_html/assets/css'
  },
  sass:       {
    errLogToConsole: true,
    outputStyle:     'compressed'
  },
  watchify:   {},
  browserify: {
    debug: true
  },
  babelify:   {
    presets: ['es2015']
  }
};

let modules = [
  'style-guide'
];

// Error reporting function
function mapError(err) {
  if (err.fileName) {
    // Regular error
    gutil.log(chalk.red(err.name)
      + ': ' + chalk.yellow(err.fileName.replace(__dirname + '/src/js/', ''))
      + ': ' + 'Line ' + chalk.magenta(err.lineNumber)
      + ' & ' + 'Column ' + chalk.magenta(err.columnNumber || err.column)
      + ': ' + chalk.blue(err.description));
  } else {
    // Browserify error..
    gutil.log(chalk.red(err.name)
      + ': '
      + chalk.yellow(err.message));
  }
}

// Completes the final file outputs
function bundle(bundler, entry) {
  let bundleTimer = duration('Javascript (custom) bundle time');

  bundler
    .bundle()
    .on('error', mapError) // Map error reporting
    .pipe(source(entry + '.js')) // Set source name
    .pipe(buffer()) // Convert to gulp pipeline
    .pipe(rename(entry + '.min.js')) // Rename the output file
    .pipe(sourcemaps.init({loadMaps: true})) // Extract the inline sourcemaps
    .pipe(uglify())
    .pipe(sourcemaps.write('./map')) // Set folder for sourcemaps to output to
    .pipe(gulp.dest(config.js.dst)) // Set the output folder
    /*
     .pipe(notify({
     message: 'Generated file: <%= file.relative %>'
     })) // Output the file being created
     */
    .pipe(bundleTimer) // Output time timing of the file creation
    .pipe(livereload()); // Reload the view in the browser
}

gulp.task('custom-js', () => {
  livereload.listen(); // Start livereload server
  let args = merge(watchify.args, config.browserify); // Merge in default watchify args with browserify arguments
  modules.map((module) => {
    let bundler = browserify(config.js.src + '/' + module + '.js', args) // Browserify
      .plugin(watchify, config.watchify) // Watchify to watch source file changes
      .transform(babelify, config.babelify) // Babel transforms
      .transform(vueify); // Vue transforms

    bundle(bundler, module); // Run the bundle the first time (required for Watchify to kick in)

    bundler.on('update', () => {
      bundle(bundler, module); // Re-run bundle on source updates
    });
  });

  // create a merged stream and return it
  // return es.merge.apply(null, tasks);
});

gulp.task('lib-js', () => {
  let bundler = browserify(config.js.src + '/libs.js');

  return bundle(bundler, 'libs');
});

gulp.task('custom-sass', () => {
  livereload.listen();
  let bundleTimer = duration('CSS custom bundle time');

  return gulp
    .src(config.css.src + '/custom/main.scss')
    .pipe(sass(config.sass).on('error', sass.logError))
    .pipe(rename('custom.css'))
    .pipe(gulp.dest(config.css.dst))

    /*
     .pipe(notify({
     message: 'Generated file: <%= file.relative %>'
     })) // Output the file being created
     */
    .pipe(bundleTimer) // Output time timing of the file creation
    .pipe(livereload());
});

gulp.task('lib-sass', () => {
  livereload.listen();
  let bundleTimer = duration('CSS libraries bundle time');

  return gulp
    .src(config.css.src + '/libs/main.scss')
    .pipe(sass(config.sass).on('error', sass.logError))
    .pipe(rename('libs.css'))
    .pipe(gulp.dest(config.css.dst))

    /*
     .pipe(notify({
     message: 'Generated file: <%= file.relative %>'
     })) // Output the file being created
     */
    .pipe(bundleTimer) // Output time timing of the file creation
    .pipe(livereload());
});

gulp.task('watch', () => {
  gulp.watch(config.css.src + '/custom/main.scss', ['custom-sass']);
  gulp.watch(config.css.src + '/libs/main.scss', ['lib-sass']);
});

gulp.task('default', ['build', 'watch']);
gulp.task('build', ['build-js', 'build-css']);
gulp.task('build-js', ['custom-js', 'lib-js']);
gulp.task('build-css', ['custom-sass', 'lib-sass']);
